﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;

namespace ReaderGpx
{
    public class GpxReader : IGpxReader
    {
        public ICollection<TrackPoint> Load(string file = "twister.gpx")
        {
            var gpx = XNamespace.Get("http://www.topografix.com/GPX/1/1");
            var gpxDoc = XDocument.Load(file);
            var trackPoints = gpxDoc.Descendants(gpx + "trkpt")
                .Select(p => new TrackPoint
                    (
                        DateTime.ParseExact(p.Element(gpx + "time")?.Value, "yyyy-MM-dd'T'HH:mm:ss'Z'", CultureInfo.InvariantCulture),
                        Convert.ToDouble(p.Element(gpx + "ele")?.Value, CultureInfo.InvariantCulture),
                        Convert.ToDouble(p.Attribute("lon")?.Value, CultureInfo.InvariantCulture),
                        Convert.ToDouble(p.Attribute("lat")?.Value, CultureInfo.InvariantCulture)
                    )
                ).ToList();
            return trackPoints;
        }
    }
}
