﻿using System.Collections.Generic;

namespace ReaderGpx
{
    public interface IGpxReader
    {
        ICollection<TrackPoint> Load(string file);
    }
}
