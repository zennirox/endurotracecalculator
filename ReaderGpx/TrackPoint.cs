﻿using System;

namespace ReaderGpx
{
    public class TrackPoint
    {
        /// <summary>
        /// Time when point was captured
        /// </summary>
        public DateTime Time { get; }
        /// <summary>
        /// Height above sea level
        /// </summary>
        public double Elevation { get; }

        /// <summary>
        /// dlugosc 
        /// </summary>
        public double Longitude { get; }
        /// <summary>
        /// szerokosć
        /// </summary>
        public double Latitude { get; }

        public TrackPoint(DateTime time, double elevation, double longitude, double latitude)
        {
            Time = time;
            Elevation = elevation;
            Longitude = longitude;
            Latitude = latitude;
        }
    }
}
