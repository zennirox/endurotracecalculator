﻿using System.Collections.Generic;
using Mathematic;
using Mathematica;
using ReaderGpx;

namespace GpxAnalizer
{
    public class GpxAnalyzerManager
    {
        public IGpxReader GpxReader { get; }
        public IDistanceCalculator DistanceCalculator { get; }
        public ISpeedCalculator SpeedCalculator { get; }
        public ITimeCalculator TimeCalculator { get; }
        public IElevationCalculator ElevatitionCalculator { get; }
        public GpxAnalyzerManager(
            IGpxReader gpxReader,
            IDistanceCalculator distanceCalculator,
            ISpeedCalculator speedCalculator,
            ITimeCalculator timeCalculator,
            IElevationCalculator elevationCalculator
            )
        {
            GpxReader = gpxReader;
            DistanceCalculator = distanceCalculator;
            SpeedCalculator = speedCalculator;
            TimeCalculator = timeCalculator;
            ElevatitionCalculator = elevationCalculator;
        }

        public ICollection<TrackPoint> ReadGpxFile(string file)
        {
            return GpxReader.Load(file);
        }
        public GpxAnalyzerManager SetDistanceType(DistanceType distanceType)
        {
            DistanceCalculator.SetDistanceType(distanceType);
            return this;
        }

        public GpxAnalyzerManager SetCurrentDateIgnoreMode(DateIgnoreType dateIgnoreType)
        {
            TimeCalculator.SetDateIgnoreMode(dateIgnoreType);
            return this;
        }
    }
}
