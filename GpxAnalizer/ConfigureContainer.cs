﻿using Autofac;
using GpxAnalizer.DataSegmenting;
using Mathematica;
using ReaderGpx;

namespace GpxAnalizer
{
    public class ConfigureContainer
    {
        private static IContainer _container;
        public static IContainer Container => _container ?? (_container = Configure());

        private static IContainer Configure()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<GpxReader>().As<IGpxReader>();
            builder.RegisterType<DistanceCalculator>().As<IDistanceCalculator>();
            builder.RegisterType<SpeedCalculator>().As<ISpeedCalculator>();
            builder.RegisterType<TimeCalculator>().As<ITimeCalculator>();
            builder.RegisterType<ElevationCalculator>().As<IElevationCalculator>();
            builder.RegisterType<DataSegmentation>().As<IDataSegmentation>();
            builder.RegisterType<GpxAnalyzerManager>();
            return builder.Build();

        }
    }
}
