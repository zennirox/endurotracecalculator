﻿using Autofac;

namespace GpxAnalizer
{
    public static class Initialize
    {
        public static GpxAnalyzerManager InitializeAnalyzerManager()
        {
            return ConfigureContainer.Container.Resolve<GpxAnalyzerManager>();
        }

    }
}
