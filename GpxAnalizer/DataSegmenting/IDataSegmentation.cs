﻿using System.Collections.Generic;
using ReaderGpx;

namespace GpxAnalizer.DataSegmenting
{
    public interface IDataSegmentation
    {
        ICollection<Segment> SegmentData(IReadOnlyList<TrackPoint> trackPoints, double secondsMaxDiff);
    }
}
