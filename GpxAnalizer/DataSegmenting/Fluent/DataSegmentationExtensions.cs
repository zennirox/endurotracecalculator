﻿using System.Collections.Generic;
using System.Linq;
using Autofac;
using ReaderGpx;

namespace GpxAnalizer.DataSegmenting.Fluent
{
    public static class DataSegmentationExtension
    {
        private static readonly IDataSegmentation _dataSegmentation
            = ConfigureContainer.Container.Resolve<IDataSegmentation>();

        public static ICollection<Segment> SegmentData(this ICollection<TrackPoint> trackPoints, int secondsMaxDiff)
        {
            return _dataSegmentation.SegmentData(trackPoints.ToArray(), secondsMaxDiff);
        }
    }
}
