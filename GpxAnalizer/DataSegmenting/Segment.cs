﻿using System.Collections.Generic;
using ReaderGpx;

namespace GpxAnalizer.DataSegmenting
{
    public class Segment
    {
        public ICollection<TrackPoint> TrackPoints { get; }

        public Segment()
        {
            TrackPoints = new List<TrackPoint>();
        }

    }
}
