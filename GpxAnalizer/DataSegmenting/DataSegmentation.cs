﻿using System.Collections.Generic;
using System.Linq;
using Mathematica;
using ReaderGpx;

namespace GpxAnalizer.DataSegmenting
{
    public class DataSegmentation : IDataSegmentation
    {
        private readonly ITimeCalculator _timeCalculator;

        public DataSegmentation(ITimeCalculator timeCalculator)
        {
            _timeCalculator = timeCalculator;
        }

        public ICollection<Segment> SegmentData(IReadOnlyList<TrackPoint> trackPoints, double secondsMaxDiff)
        {
            var segmentData = new List<Segment>(trackPoints.Count);
            segmentData.Add(new Segment());
            var currectSegmentIndex = 0;
            for (int i = 0; i < trackPoints.Count; i++)
            {
                segmentData[currectSegmentIndex].TrackPoints.Add(trackPoints[i]);
                if (i == trackPoints.Count - 1)
                {
                    break;
                }

                if (!(_timeCalculator.GetTime(trackPoints[i], trackPoints[i + 1]).TotalSeconds >
                      secondsMaxDiff)) continue;

                currectSegmentIndex++;
                segmentData.Add(new Segment());
            }
            segmentData.TrimExcess();
            return segmentData;
        }
    }

    
}
