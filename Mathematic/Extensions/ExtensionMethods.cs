﻿using System;
using System.Collections.Generic;
using System.Linq;
using ReaderGpx;

namespace Mathematic.Extensions
{
    internal static class ExtensionMethods
    {
        public static double ToRadians(this double value)
        {
            return Math.PI * value / 180.0;
        }
        public static double ToMeters(this double kilometers)
        {
            return kilometers * 1000;
        }

        public static void ThrowIfNotValid(this IReadOnlyCollection<TrackPoint> trackPoints)
        {
            if (trackPoints == null)
                throw new ArgumentNullException(nameof(trackPoints));
            if (trackPoints.Any(tr => tr == null))
                throw new InvalidOperationException("Collection contains nulls");
            if (trackPoints.Count < 2)
                throw new InvalidOperationException("Collection of passed points must contains at least 2 points");
        }
    }
}
