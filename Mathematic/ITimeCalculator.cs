﻿using System;
using System.Collections.Generic;
using ReaderGpx;

namespace Mathematica
{
    public interface ITimeCalculator
    {
        void SetDateIgnoreMode(DateIgnoreType ignore);
        TimeSpan GetTotalTime(ICollection<TrackPoint> trackPoints);
        TimeSpan GetClimbingTime(ICollection<TrackPoint> trackPoints);
        TimeSpan GetDescentTime(ICollection<TrackPoint> trackPoints);
        TimeSpan GetFlatTime(ICollection<TrackPoint> trackPoints);
        TimeSpan GetTime(TrackPoint first, TrackPoint second);
        List<List<TimeSpan>> GetTimeSpansBetweenPoints(IReadOnlyList<TrackPoint> trackPoints);
    }

    public enum DateIgnoreType
    {
        Date,
        Nothing,
    }
}
