﻿using System.Collections.Generic;
using ReaderGpx;

namespace Mathematica
{
    public interface IDistanceCalculator
    {
        void SetDistanceType(DistanceType distanceType);
        double GetTotalDistance(ICollection<TrackPoint> trackPoints);
        double GetClimbingDistance(ICollection<TrackPoint> trackPoints);
        double GetDescentDistance(ICollection<TrackPoint> trackPoints);
        double GetFlatDistance(ICollection<TrackPoint> trackPoints);
        double HaversineFormula(TrackPoint first, TrackPoint second);
    }
}
