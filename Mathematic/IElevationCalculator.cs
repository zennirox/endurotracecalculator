﻿using ReaderGpx;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mathematica
{
    public interface IElevationCalculator
    {
        double GetMinimumElevation(ICollection<TrackPoint> trackPoints);
        double GetMaximumElevation(ICollection<TrackPoint> trackPoints);
        double GetAverageElevation(ICollection<TrackPoint> trackPoints);
        double GetTotalLoss(ICollection<TrackPoint> trackPoints);
        double GetTotalGain(ICollection<TrackPoint> trackPoints);
        double GetFinalBalance(ICollection<TrackPoint> trackPoints);
    }
}
