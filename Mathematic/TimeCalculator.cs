﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mathematic;
using Mathematic.Extensions;
using ReaderGpx;

namespace Mathematica
{
    public class TimeCalculator : ITimeCalculator
    {
        public DateIgnoreType CurrentDateIgnoreMode { get; private set; } = DateIgnoreType.Nothing;
        public void SetDateIgnoreMode(DateIgnoreType ignore)
        {
            CurrentDateIgnoreMode = ignore;
        }
        public TimeSpan GetTime(TrackPoint first, TrackPoint second)
        {
            return CurrentDateIgnoreMode == DateIgnoreType.Date 
                ? (second.Time.TimeOfDay - first.Time.TimeOfDay).Duration() 
                : (second.Time - first.Time).Duration();
        }

        public TimeSpan GetTotalTime(ICollection<TrackPoint> trackPoints)
        {
            return GetTime(trackPoints.ToList());
        }
        public TimeSpan GetClimbingTime(ICollection<TrackPoint> trackPoints)
        {
            return GetTime(trackPoints.ToList(), (x, y) => x.Elevation < y.Elevation);
        }
        public TimeSpan GetDescentTime(ICollection<TrackPoint> trackPoints)
        {
            return GetTime(trackPoints.ToList(), (x, y) => x.Elevation > y.Elevation);
        }

        public TimeSpan GetFlatTime(ICollection<TrackPoint> trackPoints)
        {
            return GetTime(trackPoints.ToList(), (x, y) => Math.Abs(x.Elevation - y.Elevation) < 0.001); //TODO: check if epsilon not too small
        }

        public TimeSpan GetTime(IReadOnlyList<TrackPoint> trackPoints, Func<TrackPoint, TrackPoint, bool> predicate = null)
        {
            trackPoints.ThrowIfNotValid();
            var total = new TimeSpan();
            for (var i = 0; i < trackPoints.Count - 1; i++)
            {
                if (predicate == null || predicate(trackPoints[i], trackPoints[i + 1]))
                {
                    total += GetTime(trackPoints[i], trackPoints[i + 1]);
                }

            }
            return total;
        }

        public List<List<TimeSpan>> GetTimeSpansBetweenPoints(IReadOnlyList<TrackPoint> trackPoints)
        {
            var groupedTrackPoints = trackPoints.GroupBy(x => x.Time.Date);
            var timeSpansCollection = new List<List<TimeSpan>>();
            foreach (var grouped in groupedTrackPoints)
            {
                var tracksPointsTimeSpanFromSameDay = new List<TimeSpan>();
                for (var i = 0; i < grouped.Count() - 1; i++)
                {
                    tracksPointsTimeSpanFromSameDay.Add(GetTime(grouped.ElementAt(i), grouped.ElementAt(i + 1)));
                }
                timeSpansCollection.Add(tracksPointsTimeSpanFromSameDay);
            }
            return timeSpansCollection;
        }
    }
}
