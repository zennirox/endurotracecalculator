﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mathematic.Extensions;
using ReaderGpx;

namespace Mathematica
{
    public class ElevationCalculator : IElevationCalculator
    {
        public double GetMinimumElevation(ICollection<TrackPoint> trackPoints)
        {
            return trackPoints.Min(x => x.Elevation);
        }

        public double GetMaximumElevation(ICollection<TrackPoint> trackPoints)
        {
            return trackPoints.Max(x => x.Elevation);
        }
        public double GetAverageElevation(ICollection<TrackPoint> trackPoints)
        {
            return trackPoints.Average(x => x.Elevation);
        }
        public double GetTotalLoss(ICollection<TrackPoint> trackPoints)
        {
            return GetElevation(trackPoints.ToList(), (x, y) => x.Elevation > y.Elevation);
        }
        public double GetTotalGain(ICollection<TrackPoint> trackPoints)
        {
            return GetElevation(trackPoints.ToList(), (x, y) => x.Elevation < y.Elevation);
        }

        public double GetFinalBalance(ICollection<TrackPoint> trackPoints)
        {
            return GetTotalGain(trackPoints) - GetTotalLoss(trackPoints);
        }


        private double GetElevation(IReadOnlyList<TrackPoint> trackPoints, Func<TrackPoint, TrackPoint, bool> predicate = null)
        {
            trackPoints.ThrowIfNotValid();

            var elevation = 0d;
            for (int i = 0; i < trackPoints.Count - 1; i++)
            {
                if (predicate == null || predicate(trackPoints[i], trackPoints[i + 1]))
                {
                    elevation += Math.Abs(trackPoints[i].Elevation - trackPoints[i + 1].Elevation);
                }
            }
            return elevation;
        }
    }
}
