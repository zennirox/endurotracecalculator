﻿using System.Collections.Generic;
using ReaderGpx;

namespace Mathematica
{
    public interface ISpeedCalculator
    {
        double GetSpeed(TrackPoint first, TrackPoint second);
        double GetAverageSpeed(ICollection<TrackPoint> trackPoints);
        double GetMaxSpeed(ICollection<TrackPoint> trackPoints);
        double GetMinSpeed(ICollection<TrackPoint> trackPoints);
        double GetFlatSpeed(ICollection<TrackPoint> trackPoints);
        double GetDescentSpeed(ICollection<TrackPoint> trackPoints);
        double GetClimbingSpeed(ICollection<TrackPoint> trackPoints);
    }
}
