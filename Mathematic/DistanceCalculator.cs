﻿using System;
using System.Collections.Generic;
using System.Linq;
using Functional.Maybe;
using Mathematic;
using Mathematic.Extensions;
using ReaderGpx;

namespace Mathematica
{
    public class DistanceCalculator : IDistanceCalculator
    {
        public DistanceType CurrentDistanceType { get; private set; } = DistanceType.Kilometers;
        public void SetDistanceType(DistanceType distanceType)
        {
            CurrentDistanceType = distanceType;
        }
        public double HaversineFormula(TrackPoint first, TrackPoint second)
        {
            if (first == null || second == null)
                throw new ArgumentNullException($"{nameof(first)} or {nameof(second)} is null");

            var dlon = (second.Longitude - first.Longitude).ToRadians();
            var dlat = (second.Latitude - first.Latitude).ToRadians();

            var a =
                Math.Pow(Math.Sin(dlat / 2), 2) +
                Math.Cos(first.Latitude.ToRadians()) * Math.Cos(second.Latitude.ToRadians()) *
                Math.Pow(Math.Sin(dlon / 2), 2);

            var asin = Math.Asin(Math.Min(1, Math.Sqrt(a)));
            if (double.IsNaN(asin))
            {
                throw new InvalidOperationException("Cant calculate. Result of Asin is NaN");
            }

            return (double)CurrentDistanceType * 2 * asin;
        }

        public double GetTotalDistance(ICollection<TrackPoint> trackPoints)
        {
            return GetDistance(trackPoints.ToList());
        }

        public double GetClimbingDistance(ICollection<TrackPoint> trackPoints)
        {
            return GetDistance(trackPoints.ToList(),(x, y) => x.Elevation < y.Elevation);
        }

        public double GetDescentDistance(ICollection<TrackPoint> trackPoints)
        {
            return GetDistance(trackPoints.ToList(),(x, y) => x.Elevation > y.Elevation);
        }
        public double GetFlatDistance(ICollection<TrackPoint> trackPoints)
        {
            return GetDistance(trackPoints.ToList(),(x, y) => Math.Abs(x.Elevation - y.Elevation) < 0.001); //TODO: check if epsilon not too small
        }

        private double GetDistance(IReadOnlyList<TrackPoint> trackPoints, Func<TrackPoint, TrackPoint, bool> predicate = null)
        {
            trackPoints.ThrowIfNotValid();

            var distance = 0d;
            for (int i = 0; i < trackPoints.Count - 1; i++)
            {
                if (predicate == null || predicate(trackPoints[i], trackPoints[i + 1]))
                {
                    distance += HaversineFormula(trackPoints[i], trackPoints[i + 1]);
                }
            }
            return distance;
        }
    }

    public enum DistanceType : int
    {
        Kilometers = 6373,
        Miles = 3961
    }

}
