﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mathematic.Extensions;
using ReaderGpx;

namespace Mathematica
{
    public class SpeedCalculator : ISpeedCalculator
    {
        private readonly IDistanceCalculator _distanceCalculator;
        private readonly ITimeCalculator _timeCalculator;

        public SpeedCalculator(
            IDistanceCalculator distanceCalculator,
            ITimeCalculator timeCalculator
        )
        {
            _distanceCalculator = distanceCalculator;
            _timeCalculator = timeCalculator;
        }
        public double GetAverageSpeed(ICollection<TrackPoint> trackPoints)
        {
            if (trackPoints == null)
                throw new ArgumentNullException(nameof(trackPoints));

            return GetSpeed2(trackPoints.ToArray(), v => v.Average());
        }

        public double GetSpeed(TrackPoint first, TrackPoint second)
        {
            if (first == null || second == null)
            {
                throw new ArgumentNullException("Null point passed");
            }
            var distance = _distanceCalculator.HaversineFormula(first, second);
            var time = _timeCalculator.GetTime(first, second).TotalHours;


            return distance / time;
        }

        public double GetMinSpeed(ICollection<TrackPoint> trackPoints)
        {
            return GetSpeed2(trackPoints.ToArray(), v => v.Min());
        }

        public double GetMaxSpeed(ICollection<TrackPoint> trackPoints)
        {
            return GetSpeed2(trackPoints.ToArray(), v => v.Max());
        }

        public double GetFlatSpeed(ICollection<TrackPoint> trackPoints)
        {
            return GetSpeed2(
                trackPoints.ToArray(),
                v => v.Average(), 
                (t1, t2) => Math.Abs(t1.Elevation - t2.Elevation) < 0.001);
        }
        public double GetDescentSpeed(ICollection<TrackPoint> trackPoints)
        {
            return GetSpeed2(
                trackPoints.ToArray(),
                v => v.Average(),
                (t1, t2) => t1.Elevation > t2.Elevation);
        }
        public double GetClimbingSpeed(ICollection<TrackPoint> trackPoints)
        {
            return GetSpeed2(
                trackPoints.ToArray(), 
                v => v.Average(), 
                (t1, t2) => t1.Elevation < t2.Elevation);
        }
        public double GetSpeed2(
            IReadOnlyList<TrackPoint> trackPoints,
            Func<ICollection<double>, double> speedCalculating, 
            Func<TrackPoint, TrackPoint, bool> predicate = null
            )
        {
            if (speedCalculating == null)
                throw new ArgumentNullException(nameof(speedCalculating) + "is not specified");
            trackPoints.ThrowIfNotValid();

            var speeds = new List<double>(trackPoints.Count);
            for (var i = 0; i < trackPoints.Count - 1; i++)
            {
                var first = trackPoints[i];
                var second = trackPoints[i + 1];
                if (predicate == null || predicate(first, second))
                {
                    var result = _distanceCalculator.HaversineFormula(first, second) /
                                 _timeCalculator.GetTime(first, second).TotalHours;
                    
                    speeds.Add(result);
                }

            }
            return speedCalculating.Invoke(speeds);
        }
    }
}
