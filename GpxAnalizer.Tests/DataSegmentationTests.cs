﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using GpxAnalizer.DataSegmenting;
using Mathematica;
using NUnit.Framework;
using ReaderGpx;

namespace GpxAnalizer.Tests
{
    [TestFixture]
    public class DataSegmentationTests
    {
        [Test]
        public void SegmentData_FourTrackPoints_ReturnsTwoSegments()
        {
            var dataSegmentation = new DataSegmentation(new TimeCalculator());
            var pointList = new List<TrackPoint>()
            {
                new TrackPoint(new DateTime(2018, 11, 13, 23, 58, 00), 0, 19.045251, 49.767436),
                new TrackPoint(new DateTime(2018, 11, 13, 23, 59, 00), 0, 19.045281, 49.767408),
                new TrackPoint(new DateTime(2018, 11, 13, 23, 59, 30), 0, 19.045281, 49.767408),
                new TrackPoint(new DateTime(2018, 11, 16, 00, 00, 00), 0, 19.045251, 49.767436),
                new TrackPoint(new DateTime(2018, 11, 16, 00, 01, 00), 0, 19.045281, 49.767408),
                new TrackPoint(new DateTime(2018, 11, 16, 00, 05, 00), 0, 19.045281, 49.767408),
            };
            var result = dataSegmentation.SegmentData(pointList, 2000);
            result.Should().HaveCount(2);
        }
        [Test]
        public void SegmentData_FourTrackPoints_SegmentsHasCorrectCount()
        {
            var dataSegmentation = new DataSegmentation(new TimeCalculator());
            var pointList = new List<TrackPoint>()
            {
                new TrackPoint(new DateTime(2018, 11, 13, 23, 58, 00), 0, 19.045251, 49.767436),
                new TrackPoint(new DateTime(2018, 11, 13, 23, 59, 00), 0, 19.045281, 49.767408),
                new TrackPoint(new DateTime(2018, 11, 13, 23, 59, 30), 0, 19.045281, 49.767408),
                new TrackPoint(new DateTime(2018, 11, 16, 00, 00, 00), 0, 19.045251, 49.767436),
                new TrackPoint(new DateTime(2018, 11, 16, 00, 01, 00), 0, 19.045281, 49.767408),
                new TrackPoint(new DateTime(2018, 11, 16, 00, 05, 00), 0, 19.045281, 49.767408),
            };
            var result = dataSegmentation.SegmentData(pointList, 2000).ToArray();
            result[0].TrackPoints.Should().HaveCount(3);
            result[1].TrackPoints.Should().HaveCount(3);
        }
    }
}
