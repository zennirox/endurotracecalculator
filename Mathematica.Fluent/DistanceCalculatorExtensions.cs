﻿using System.Collections.Generic;
using Autofac;
using GpxAnalizer;
using Mathematic;
using ReaderGpx;

namespace Mathematica.Fluent
{
    public static class DistanceCalculatorExtensions
    {
        private static readonly IDistanceCalculator _distanceCalculator 
            = ConfigureContainer.Container.Resolve<IDistanceCalculator>();
        public static double GetTotalDistance(this ICollection<TrackPoint> trackPoints)
        {
            return _distanceCalculator.GetTotalDistance(trackPoints);
        }
        public static double GetClimbingDistance(this ICollection<TrackPoint> trackPoints)
        {
            return _distanceCalculator.GetClimbingDistance(trackPoints);
        }
        public static double GetDescentDistance(this ICollection<TrackPoint> trackPoints)
        {
            return _distanceCalculator.GetDescentDistance(trackPoints);
        }
        public static double GetFlatDistance(this ICollection<TrackPoint> trackPoints)
        {
            return _distanceCalculator.GetFlatDistance(trackPoints);
        }

        public static double GetDistance(this TrackPoint first, TrackPoint to)
        {
            return _distanceCalculator.HaversineFormula(first, to);
        }
    }
}
