﻿using System;
using System.Collections.Generic;
using Autofac;
using GpxAnalizer;
using Mathematic;
using ReaderGpx;

namespace Mathematica.Fluent
{
    public static class TimeCalculatorExtensions
    {
        private static readonly ITimeCalculator _timeCalculator 
            = ConfigureContainer.Container.Resolve<ITimeCalculator>();

        public static TimeSpan GetTotalTime(this ICollection<TrackPoint> trackPoints)
        {
            return _timeCalculator.GetTotalTime(trackPoints);
        }

        public static TimeSpan GetClimbingTime(this ICollection<TrackPoint> trackPoints)
        {
            return _timeCalculator.GetClimbingTime(trackPoints);
        }
        public static TimeSpan GetDescentTime(this ICollection<TrackPoint> trackPoints)
        {
            return _timeCalculator.GetDescentTime(trackPoints);
        }
        public static TimeSpan GetFlatTime(this ICollection<TrackPoint> trackPoints)
        {
            return _timeCalculator.GetFlatTime(trackPoints);
        }
    }
}
