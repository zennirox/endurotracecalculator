﻿using System;
using System.Collections.Generic;
using Autofac;
using GpxAnalizer;
using Mathematic;
using ReaderGpx;

namespace Mathematica.Fluent
{
    public static class ElevationCalculatorExtensions
    {
        private static readonly IElevationCalculator _elevationCalculator
    = ConfigureContainer.Container.Resolve<IElevationCalculator>();
        public static double getMinimumElevation(this ICollection<TrackPoint> trackPoints)
        {
            return _elevationCalculator.GetMinimumElevation(trackPoints);
        }

        public static double getMaximumElevation(this ICollection<TrackPoint> trackPoints)
        {
            return _elevationCalculator.GetMaximumElevation(trackPoints);
        }
        public static double GetAverageElevation(this ICollection<TrackPoint> trackPoints)
        {
            return _elevationCalculator.GetAverageElevation(trackPoints);
        }
        public static double GetTotalLoss(this ICollection<TrackPoint> trackPoints)
        {
            return _elevationCalculator.GetTotalLoss(trackPoints);
        }
        public static double GetTotalGain(this ICollection<TrackPoint> trackPoints)
        {
            return _elevationCalculator.GetTotalGain(trackPoints);
        }
        public static double GetFinalBalance(this ICollection<TrackPoint> trackPoints)
        {
            return _elevationCalculator.GetFinalBalance(trackPoints);
        }
    }
}
