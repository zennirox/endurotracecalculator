﻿using System.Collections.Generic;
using Autofac;
using GpxAnalizer;
using Mathematic;
using ReaderGpx;

namespace Mathematica.Fluent
{
    public static class SpeedCalculatorExtensions
    {
        private static readonly ISpeedCalculator _speedCalculator
            = ConfigureContainer.Container.Resolve<ISpeedCalculator>();

        public static double GetAverageSpeed(this ICollection<TrackPoint> trackPoints)
        {
            return _speedCalculator.GetAverageSpeed(trackPoints);
        }
        public static double GetMaxSpeed(this ICollection<TrackPoint> trackPoints)
        {
            return _speedCalculator.GetMaxSpeed(trackPoints);
        }
        public static double GetMinSpeed(this ICollection<TrackPoint> trackPoints)
        {
            return _speedCalculator.GetMinSpeed(trackPoints);
        }
        public static double GetFlatSpeed(this ICollection<TrackPoint> trackPoints)
        {
            return _speedCalculator.GetFlatSpeed(trackPoints);
        }
        public static double GetDescentSpeed(this ICollection<TrackPoint> trackPoints)
        {
            return _speedCalculator.GetDescentSpeed(trackPoints);
        }
        public static double GetClimbingSpeed(this ICollection<TrackPoint> trackPoints)
        {
            return _speedCalculator.GetClimbingSpeed(trackPoints);
        }
    }
}
