﻿using System;
using FluentAssertions;
using Mathematica;
using NUnit.Framework;
using ReaderGpx;

namespace Mathematic.Tests
{
    [TestFixture]
    public class DistanceCalculatorTests
    {
        [Test]
        public void HaversineFormula_TwoTrackPointsKilometers_ReturnsCorrectValue()
        {
            var point1 = new TrackPoint(DateTime.Now, 0, 19.045251, 49.767436);
            var point2 = new TrackPoint(DateTime.Now, 0, 19.045281, 49.767408);

            var calculator = new DistanceCalculator();
            var result = calculator.HaversineFormula(point1, point2);

            var dif = Math.Abs(result - 0.004449);
            dif.Should().BeLessThan(0.001);
        }
        [Test]
        public void HaversineFormula_TwoNullPoints_ThrowsNullException()
        {
            var calculator = new DistanceCalculator();

            Action action = () => calculator.HaversineFormula(null,null);
            action.Should().Throw<ArgumentNullException>();
        }

        [Test]
        public void TotalDistance_TwoTrackPointsKilometers_ReturnsCorrectValue()
        {
            var point1 = new TrackPoint(DateTime.Now, 0, 19.045251, 49.767436);
            var point2 = new TrackPoint(DateTime.Now, 0, 19.045281, 49.767408);
            var calculator = new DistanceCalculator();
            var result = calculator.GetTotalDistance(new []{point1, point2});
            var dif = Math.Abs(result - 0.004449);
            dif.Should().BeLessThan(0.001);
        }

        [Test]
        public void TotalDistance_PointsFromFile_ReturnsCorrectValue()
        {
            // TODO: replace this cancer path or add file to our solution
            var points = new GpxReader().Load(@"..\..\..\..\..\twister.gpx");

            var calculator = new DistanceCalculator();
            var result = calculator.GetTotalDistance(points);

            var dif = Math.Abs(result - 4.1);
            dif.Should().BeLessThan(0.1);
        }


        [Test]
        public void ClimbingDistance_PointsFromFile_ReturnsCorrectValue()
        {
            var points = new GpxReader().Load(@"..\..\..\..\..\twister.gpx");

            var calculator = new DistanceCalculator();
            var result = calculator.GetClimbingDistance(points);

            var dif = Math.Abs(result - 0.9);
            dif.Should().BeLessThan(0.1);
        }

        [Test]
        public void DescentDistance_PointsFromFile_ReturnsCorrectValue()
        {
            var points = new GpxReader().Load(@"..\..\..\..\..\twister.gpx");

            var calculator = new DistanceCalculator();
            var result = calculator.GetDescentDistance(points);

            var dif = Math.Abs(result - 2.7);
            dif.Should().BeLessThan(0.1);
        }


        [Test]
        public void FlatDistance_PointsFromFile_ReturnsCorrectValue()
        {
            var points = new GpxReader().Load(@"..\..\..\..\..\twister.gpx");

            var calculator = new DistanceCalculator();
            var result = calculator.GetFlatDistance(points);

            var dif = Math.Abs(result - 0.4);
            dif.Should().BeLessThan(0.1);
        }

        [Test]
        public void TotalDistanceCompareWithSumDistances_PointsFromFile_ReturnsCorrectValue()
        {
            var points = new GpxReader().Load(@"..\..\..\..\..\twister.gpx");

            var calculator = new DistanceCalculator();
            var sumDistance = calculator.GetClimbingDistance(points)+calculator.GetDescentDistance(points) + calculator.GetFlatDistance(points);
            var totalDistance = calculator.GetTotalDistance(points);
            var dif = Math.Abs(totalDistance - sumDistance);
            dif.Should().BeLessThan(0.0000001);
        }
    }
}
