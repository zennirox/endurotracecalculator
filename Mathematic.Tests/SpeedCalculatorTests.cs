﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using GpxAnalizer.DataSegmenting;
using Mathematica;
using NUnit.Framework;
using ReaderGpx;

namespace Mathematic.Tests
{
    [TestFixture]
    public class SpeedCalculatorTests
    {
        [Test]
        public void GetAverageSpeed_PointsFromFile_ReturnsCorrectValue()
        {
            var points = new GpxReader().Load(@"..\..\..\..\..\twister.gpx");
            var calculator = new SpeedCalculator(
                new DistanceCalculator(),
                new TimeCalculator()
            );

            var result = calculator.GetAverageSpeed(points);
            var dif = Math.Abs(result - 5.9);
            dif.Should().BeLessThan(0.1);
        }
        [Test]
        public void GetAverageSpeed_TwoTackPoints_ReturnsCorrectValue()
        {
            var point1 = new TrackPoint(new DateTime(2018, 11, 13, 15, 30, 00), 0, 19.045251, 49.767436);
            var point2 = new TrackPoint(new DateTime(2018, 11, 13, 15, 35, 30), 0, 19.045281, 49.767408);

            var calculator = new SpeedCalculator(
                new DistanceCalculator(),
                new TimeCalculator()
            );
            
            var result = calculator.GetAverageSpeed(new[] { point1, point2 });
            var dif = Math.Abs(result - 0.049);
            dif.Should().BeLessThan(0.1);
        }
        [Test]
        public void GetAverageSpeed_ThreeTrackPoints_ReturnsCorrectValue()
        {
            var point1 = new TrackPoint(new DateTime(2018, 11, 13, 15, 30, 00), 0, 19.045251, 49.767436);
            var point2 = new TrackPoint(new DateTime(2018, 11, 13, 15, 35, 30), 0, 19.045281, 49.767408);
            var point3 = new TrackPoint(new DateTime(2018, 11, 14, 15, 40, 30), 0, 19.045311, 49.767386);

            var calculator = new SpeedCalculator(
                new DistanceCalculator(), 
                new TimeCalculator()
            );

            var result = calculator.GetAverageSpeed(new[] {point1, point2, point3});

            var dif = Math.Abs(result - 0.059);
            dif.Should().BeLessThan(0.1);
        }
        [Test]
        public void GetMinSpeed_ThreeTrackPoints_ReturnsCorrectValue()
        {
            var point1 = new TrackPoint(new DateTime(2018, 11, 13, 15, 30, 00), 0, 19.045251, 49.767436);
            var point2 = new TrackPoint(new DateTime(2018, 11, 13, 15, 35, 30), 0, 19.045281, 49.767408);
            var point3 = new TrackPoint(new DateTime(2018, 11, 14, 15, 40, 30), 0, 19.045311, 49.767386);

            var calculator = new SpeedCalculator(
                new DistanceCalculator(),
                new TimeCalculator()
            );

            var result = calculator.GetMinSpeed(new[] { point1, point2, point3 });

            var dif = Math.Abs(result - 0.059);
            dif.Should().BeLessThan(0.1);
        }
        [Test]
        public void GetSpeed_TwoNullPoints_ThrowsNullException()
        {
            var calculator = new SpeedCalculator(
                new DistanceCalculator(),
                new TimeCalculator()
                );

            Action action = () => calculator.GetSpeed(null, null);
            action.Should().Throw<ArgumentNullException>();
        }
        [Test]
        public void GetMaxSpeed_PointsFromFile_ReturnsCorrectValue()
        {
            var points = new GpxReader().Load(@"..\..\..\..\..\twister.gpx");
            var timeCalc = new TimeCalculator();
            timeCalc.SetDateIgnoreMode(DateIgnoreType.Date);
            var calculator = new SpeedCalculator(
                new DistanceCalculator(),
                timeCalc
            );

            var result = calculator.GetMaxSpeed(points);

            var dif = Math.Abs(result - 24.9);
            dif.Should().BeLessThan(0.1);
        }
        [Test]
        public void GetMinSpeed_PointsFromFile_ReturnsCorrectValue()
        {
            var points = new GpxReader().Load(@"..\..\..\..\..\twister.gpx");
            var timeCalc = new TimeCalculator();
            var dataSegmentation = new DataSegmentation(timeCalc);
            var data = dataSegmentation.SegmentData(points.ToArray(), 3600);

            

            timeCalc.SetDateIgnoreMode(DateIgnoreType.Date);
            var calculator = new SpeedCalculator(
                new DistanceCalculator(),
                timeCalc
            );
            var speeds = new List<double>();
            foreach (var segment in data)
            {
                speeds.Add(calculator.GetMinSpeed(segment.TrackPoints));
            }

            var result = speeds.Min();
            var dif = Math.Abs(result - 2.2);
            dif.Should().BeLessThan(0.1);
        }
        [Test]
        public void GetFlatSpeed_PointsFromFile_ReturnsCorrectValue()
        {
            var points = new GpxReader().Load(@"..\..\..\..\..\twister.gpx");
            var timeCalc = new TimeCalculator();
            timeCalc.SetDateIgnoreMode(DateIgnoreType.Date);
            var calculator = new SpeedCalculator(
                new DistanceCalculator(),
                timeCalc
            );

            var result = calculator.GetFlatSpeed(points);

            var dif = Math.Abs(result - 5.5);
            dif.Should().BeLessThan(0.1);
        }
        [Test]
        public void GetDescentSpeed_PointsFromFile_ReturnsCorrectValue()
        {
            var points = new GpxReader().Load(@"..\..\..\..\..\twister.gpx");
            var timeCalc = new TimeCalculator();
            timeCalc.SetDateIgnoreMode(DateIgnoreType.Date);
            var calculator = new SpeedCalculator(
                new DistanceCalculator(),
                timeCalc
            );

            var result = calculator.GetDescentSpeed(points);

            var dif = Math.Abs(result - 6);
            dif.Should().BeLessThan(0.1);
        }
        [Test]
        public void GetClimbingSpeed_PointsFromFile_ReturnsCorrectValue()
        {
            var points = new GpxReader().Load(@"..\..\..\..\..\twister.gpx");
            var timeCalc = new TimeCalculator();
            timeCalc.SetDateIgnoreMode(DateIgnoreType.Date);
            var calculator = new SpeedCalculator(
                new DistanceCalculator(),
                timeCalc
            );

            var result = calculator.GetClimbingSpeed(points);

            var dif = Math.Abs(result - 8.6);
            dif.Should().BeLessThan(0.1);
        }
    }
}
