﻿using FluentAssertions;
using NUnit.Framework;
using ReaderGpx;
using System;
using Mathematica;

namespace Mathematic.Tests
{
    [TestFixture]
    class TimeCalculatorTests
    {
        [Test]
        public void GetTotalTime_TwoTrackPoints_ReturnsCorrectValueIgnoreDate()
        {
            var point1 = new TrackPoint(new DateTime(2018, 11, 13, 15, 30, 00), 0, 19.045251, 49.767436);
            var point2 = new TrackPoint(new DateTime(2018, 11, 14, 15, 35, 30), 0, 19.045281, 49.767408);

            var calculator = new TimeCalculator();
            calculator.SetDateIgnoreMode(DateIgnoreType.Date);
            var result = calculator.GetTotalTime(new[] { point1, point2 }).TotalSeconds;

            var dif = Math.Abs(result - 330);
            dif.Should().BeLessThan(0.1);
        }
        [Test]
        public void GetClimbingTime_FourTrackPoints_ReturnsCorrectValueIgnoreDate()
        {
            var point1 = new TrackPoint(new DateTime(2018, 11, 13, 15, 30, 00), 500, 19.045251, 49.767436);
            var point2 = new TrackPoint(new DateTime(2018, 11, 14, 15, 35, 30), 400, 19.045281, 49.767408);
            var point3 = new TrackPoint(new DateTime(2018, 11, 14, 15, 40, 00), 600, 19.045251, 49.767436);
            var point4 = new TrackPoint(new DateTime(2018, 11, 15, 15, 45, 30), 500, 19.045281, 49.767408);

            var calculator = new TimeCalculator();
            calculator.SetDateIgnoreMode(DateIgnoreType.Date);
            var result = calculator.GetClimbingTime(new[] { point1, point2,point3,point4 }).TotalSeconds;

            var dif = Math.Abs(result - 270);
            dif.Should().BeLessThan(0.1);
        }

        [Test]
        public void GetTotalTime_TwoTrackPoints_ReturnsCorrectValueInHoursIgnoreDate()
        {
            var point1 = new TrackPoint(new DateTime(2018, 11, 13, 15, 30, 00), 0, 19.045251, 49.767436);
            var point2 = new TrackPoint(new DateTime(2019, 11, 13, 16, 35, 30), 0, 19.045281, 49.767408);

            var calculator = new TimeCalculator();
            calculator.SetDateIgnoreMode(DateIgnoreType.Date);
            var result = calculator.GetTotalTime(new[] { point1, point2 }).TotalHours;

            var dif = Math.Abs(result - 1.09);
            dif.Should().BeLessThan(0.01);
        }

        [Test]
        public void GetTotalTime_TwoTrackPoints_ReturnsCorrectValueInHours()
        {
            var point1 = new TrackPoint(new DateTime(2018, 11, 13, 23, 30, 00), 0, 19.045251, 49.767436);
            var point2 = new TrackPoint(new DateTime(2018, 11, 14, 00, 30, 00), 0, 19.045281, 49.767408);

            var calculator = new TimeCalculator();
            var result = calculator.GetTotalTime(new[] { point1, point2 }).TotalHours;

            var dif = Math.Abs(result - 1.0);
            dif.Should().BeLessThan(0.01);
        }
        [Test]
        public void GetTimeSpans_SixTrackPoints_ReturnsCorrectValue()
        {
            var point1 = new TrackPoint(new DateTime(2018, 11, 13, 23, 58, 00), 0, 19.045251, 49.767436);
            var point2 = new TrackPoint(new DateTime(2018, 11, 13, 23, 59, 00), 0, 19.045281, 49.767408);
            var point3 = new TrackPoint(new DateTime(2018, 11, 13, 23, 59, 30), 0, 19.045281, 49.767408);
            var point4 = new TrackPoint(new DateTime(2018, 11, 14, 00, 00, 00), 0, 19.045251, 49.767436);
            var point5 = new TrackPoint(new DateTime(2018, 11, 14, 00, 01, 00), 0, 19.045281, 49.767408);
            var point6 = new TrackPoint(new DateTime(2018, 11, 14, 00, 05, 00), 0, 19.045281, 49.767408);

            var calculator = new TimeCalculator();
            var result = calculator.GetTimeSpansBetweenPoints(new[] { point1, point2, point3, point4, point5, point6 });
            var sum1 = (result[0][0] + result[0][1]).TotalSeconds;
            sum1.Should().Be(90);
            var sum2 = (result[1][0] + result[1][1]).TotalSeconds;
            sum2.Should().Be(300);
        }
    }
}
