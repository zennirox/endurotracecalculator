﻿using NUnit.Framework;
using FluentAssertions;
using ReaderGpx;
using System;
using Mathematica;

namespace Mathematic.Tests
{
    [TestFixture]
    class ElevationCalculatorTests
    {
        [Test]
        public void GetMaximumElevation_TwoTrackPoints_ReturnsCorrectValue()
        {
            var point1 = new TrackPoint(new DateTime(2018, 11, 13, 15, 30, 00), 100.34, 19.045251, 49.767436);
            var point2 = new TrackPoint(new DateTime(2018, 11, 14, 15, 35, 30), 200.54, 19.045281, 49.767408);

            var calculator = new ElevationCalculator();
            var result = calculator.GetMaximumElevation(new[] { point1, point2 });

            result.Should().Equals(200.54);
        }
        [Test]
        public void GetMinimumElevation_TwoTrackPoints_ReturnsCorrectValue()
        {
            var point1 = new TrackPoint(new DateTime(2018, 11, 13, 15, 30, 00), 100.34, 19.045251, 49.767436);
            var point2 = new TrackPoint(new DateTime(2018, 11, 14, 15, 35, 30), 200.54, 19.045281, 49.767408);

            var calculator = new ElevationCalculator();
            var result = calculator.GetMinimumElevation(new[] { point1, point2 });

            result.Should().Equals(100.34);
        }
        [Test]
        public void GetAverageElevation_TwoTrackPoints_ReturnsCorrectValue()
        {
            var point1 = new TrackPoint(new DateTime(2018, 11, 13, 15, 30, 00), 100.34, 19.045251, 49.767436);
            var point2 = new TrackPoint(new DateTime(2018, 11, 14, 15, 35, 30), 200.54, 19.045281, 49.767408);

            var calculator = new ElevationCalculator();
            var result = calculator.GetAverageElevation(new[] { point1, point2 });

            result.Should().Equals(150.44);
        }

        [Test]
        public void GetTotalLoss_FourTrackPoints_ReturnsCorrectValue()
        {
            var point1 = new TrackPoint(new DateTime(2018, 11, 13, 15, 30, 00), 100.54, 19.045251, 49.767436);
            var point2 = new TrackPoint(new DateTime(2018, 11, 14, 15, 35, 30), 200.54, 19.045281, 49.767408);
            var point3 = new TrackPoint(new DateTime(2018, 11, 13, 15, 30, 00), 100.54, 19.045251, 49.767436);
            var point4 = new TrackPoint(new DateTime(2018, 11, 14, 15, 35, 30), 200.54, 19.045281, 49.767408);

            var calculator = new ElevationCalculator();
            var result = calculator.GetTotalLoss(new[] { point1, point2, point3, point4 });

            result.Should().Equals(200);
        }

        [Test]
        public void GetTotalGain_FourTrackPoints_ReturnsCorrectValue()
        {
            var point1 = new TrackPoint(new DateTime(2018, 11, 13, 15, 30, 00), 100.54, 19.045251, 49.767436);
            var point2 = new TrackPoint(new DateTime(2018, 11, 14, 15, 35, 30), 150.54, 19.045281, 49.767408);
            var point3 = new TrackPoint(new DateTime(2018, 11, 13, 15, 30, 00), 100.54, 19.045251, 49.767436);
            var point4 = new TrackPoint(new DateTime(2018, 11, 14, 15, 35, 30), 224.54, 19.045281, 49.767408);

            var calculator = new ElevationCalculator();
            var result = calculator.GetTotalLoss(new[] { point1, point2, point3, point4 });

            result.Should().Equals(274);
        }

        [Test]
        public void GetTotalBalance_FourTrackPoints_ReturnsCorrectValue()
        {
            var point1 = new TrackPoint(new DateTime(2018, 11, 13, 15, 30, 00), 100.54, 19.045251, 49.767436);
            var point2 = new TrackPoint(new DateTime(2018, 11, 14, 15, 35, 30), 150.54, 19.045281, 49.767408);
            var point3 = new TrackPoint(new DateTime(2018, 11, 13, 15, 30, 00), 100.54, 19.045251, 49.767436);
            var point4 = new TrackPoint(new DateTime(2018, 11, 14, 15, 35, 30), 224.54, 19.045281, 49.767408);

            var calculator = new ElevationCalculator();
            var result = calculator.GetTotalLoss(new[] { point1, point2, point3, point4 });

            result.Should().Equals(124);
        }
    }
}
